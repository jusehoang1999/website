import { Branch } from "./branch";

export interface Customer {
    id: number;
    customerName: string;
    debtLimit : number;                     
}