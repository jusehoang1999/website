import { Customer } from "./customer";

export interface Branch {
    id: number;
    branchName: string;
    customer: Customer["id"];
}