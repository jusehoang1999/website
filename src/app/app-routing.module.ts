
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CeativeOrderComponent } from './ceative-order/ceative-order.component';
const routes: Routes = [
  {
     path: "",
     component: CeativeOrderComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
