import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { ThongtinkhachhangComponent } from './ceative-order/thongtinkhachhang/thongtinkhachhang.component';
import { CeativeOrderComponent } from './ceative-order/ceative-order.component';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzInputModule } from 'ng-zorro-antd/input';
import { ChitiethoadonComponent } from './ceative-order/chitiethoadon/chitiethoadon.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { DonHopDongComponent } from './ceative-order/don-hop-dong/don-hop-dong.component';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzButtonSize } from 'ng-zorro-antd/button';
import { NzImageModule } from 'ng-zorro-antd/image';
import { DonKhuyenMaiComponent } from './ceative-order/don-khuyen-mai/don-khuyen-mai.component';
import { DonKhongHopDongComponent } from './ceative-order/don-khong-hop-dong/don-khong-hop-dong.component';
import { DonSanPhamMoiComponent } from './ceative-order/don-san-pham-moi/don-san-pham-moi.component';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    CeativeOrderComponent,
    ThongtinkhachhangComponent,
    ChitiethoadonComponent,
    DonHopDongComponent,
    DonKhuyenMaiComponent,
    DonKhongHopDongComponent,
    DonSanPhamMoiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzLayoutModule,
    NzBreadCrumbModule,
    NzIconModule,
    NzMenuModule,
    NzButtonModule,
    NzCollapseModule,
    NzGridModule,
    NzFormModule,
    NzSelectModule,
    NzInputModule,
    NzTableModule,
    NzCheckboxModule,
    NzImageModule,
    NzDatePickerModule,
    NzTabsModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
