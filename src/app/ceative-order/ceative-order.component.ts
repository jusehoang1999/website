import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ceative-order',
  templateUrl: './ceative-order.component.html',
  styleUrls: ['./ceative-order.component.css']
})
export class CeativeOrderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  isCollapsed = false
  toggleCollapsed() {
    this.isCollapsed = !this.isCollapsed;
  }
}
