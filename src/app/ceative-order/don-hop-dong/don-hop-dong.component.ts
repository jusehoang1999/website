import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-don-hop-dong',
  templateUrl: './don-hop-dong.component.html',
  styleUrls: ['./don-hop-dong.component.css']
})
export class DonHopDongComponent implements OnInit {
  formHopDong: FormGroup;
  constructor(private readonly fb: FormBuilder) {
    this.formHopDong = this.fb.group({
      contractId: ["", Validators.required],
      areaId: ["", Validators.required],
      productId: ["", Validators.required],
      shipPointId: ["", Validators.required],
      priceId: ["", Validators.required],
      rmooc: [""],
      driverName: ["", Validators.required],
      vehicle: ["", Validators.required],
      transportMethodId: ["", Validators.required],
      quantity2: ["", Validators.required],
      unitPrice: ["", Validators.required],
      currencyCode: ["", Validators.required],
      description: [""],
      checked: [true],
      locationCode: [""],
      productionLine: [""],
      weightLimit: [null]
    })
   }

  ngOnInit(): void {
  }
  onSubmit(){
    console.log(this.formHopDong.value);
  }
  checked = false;
  dateFormat = 'dd/MM/yyyy'
}
